import React, { useEffect, useRef, useState } from 'react';
import './App.css';
import { createChart, CrosshairMode } from 'lightweight-charts';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Box from '@mui/material/Box';


function App() {

    const chartContainerRef = useRef();
    const chart = useRef();
    const resizeObserver = useRef();

    const [chartTime, setChartTime] = useState('HOUR_1')
    const [lastSerie, setLastSerie] = useState()
    const [lineSerie, setLineSerie] = useState()
    const [itemsList, setItemsList] = useState([])
    const [inputValue, setInputValue] = useState('');

    useEffect(() => {
        if (chart.current) return
        fetchItems()
        chart.current = createChart(chartContainerRef.current, {
            width: chartContainerRef.current.clientWidth,
            height: chartContainerRef.current.clientHeight,
            layout: {
                backgroundColor: '#253248',
                textColor: 'rgba(255, 255, 255, 0.9)',
            },
            grid: {
                vertLines: {
                    color: '#334158',
                },
                horzLines: {
                    color: '#334158',
                },
            },
            crosshair: {
                mode: CrosshairMode.Normal,
            },
            priceScale: {
                borderColor: '#485c7b',
            },
            timeScale: {
                borderColor: '#485c7b',
                timeVisible: true,
                secondsVisible: false,
            },
        });

    }, [])

    useEffect(() => {
        if (itemsList.length > 0)
            fetchPrice()
    }, [chartTime, inputValue, itemsList])




    const fetchItems = () => {
        fetch(`${process.env.REACT_APP_BACKENDROUTE}/v1/resources`)
            .then(r => r.json())
            .then(result => {
                let formated = result.map(it => {
                    return { label: it.name, db_letter: it.db_letter, transportation: it.transportation, image: it.image }
                })
                setItemsList(formated)
            })
    }

    const getCurrentItemDetail = () => {
        return itemsList.filter(it => it.label == inputValue)[0]
    }

    const fetchPrice = () => {
        let itemDetail = getCurrentItemDetail()
        if (!itemDetail) return;

        fetch(`${process.env.REACT_APP_BACKENDROUTE}/v1/price/${chartTime}/${itemDetail.db_letter}`)
            .then(r => r.json())
            .then(result => {
                let formated = result.map(it => {
                    let date = new Date(it.date)
                    let delta = date.getTimezoneOffset() * 60 * -1
                    let ts = (date.getTime() / 1000) + delta
                    return { time: Math.floor(ts), open: it.o, high: it.h, low: it.l, close: it.c }
                })
                setChartData(formated)
            })
    }

    const setChartData = (datas) => {
        if (lastSerie) {
            chart.current.removeSeries(lastSerie)
        }
        let newSerie = chart.current.addCandlestickSeries({
            upColor: '#4bffb5',
            downColor: '#ff4976',
            borderDownColor: '#ff4976',
            borderUpColor: '#4bffb5',
            wickDownColor: '#838ca1',
            wickUpColor: '#838ca1',
        });
        newSerie.setData(datas);

        setLastSerie(newSerie)
        computeRentability(datas)
    }

    const computeRentability = (datas) => {
        
        if (lineSerie) {
            chart.current.removeSeries(lineSerie)
        }
        let itemDetail = getCurrentItemDetail()
        let transportationCost = 0.35 * itemDetail.transportation
        
        let rentabilityData = datas.map(it => {

            let minPriceSell = 100 * it.close / 97

            return {
                time: it.time,
                value: minPriceSell + transportationCost
            }

        })

        let newSerie = chart.current.addLineSeries({
            color: 'rgba(200, 200, 200, 0.3)',
            lineWidth: 0.5,
        });
        newSerie.setData(rentabilityData);
        setLineSerie(newSerie)
    }


    return (
        <div className="App">
            <div className='overlay'>
                <div className='time-overlay'>
                    <Button disabled={chartTime == 'MINUTE_5'} onClick={() => { setChartTime('MINUTE_5') }} >5M</Button>
                    <Button disabled={chartTime == 'MINUTE_15'} onClick={() => { setChartTime('MINUTE_15') }} >15M</Button>
                    <Button disabled={chartTime == 'HOUR_1'} onClick={() => { setChartTime('HOUR_1') }} >1H</Button>
                    <Button disabled={chartTime == 'HOUR_4'} onClick={() => { setChartTime('HOUR_4') }} >4H</Button>
                    <Button disabled={chartTime == 'DAY'} onClick={() => { setChartTime('DAY') }} >D</Button>
                </div>
                <div className='input-overlay'>
                    <Autocomplete
                        id="combo-box"
                        options={itemsList}
                        sx={{ width: 300 }}
                        inputValue={inputValue}
                        defaultValue='Apples'
                        onInputChange={(event, newInputValue) => {
                            setInputValue(newInputValue);
                        }}
                        renderInput={(params) => <TextField {...params} label="Item" />}
                        renderOption={(props, option) => (
                            <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                                <img
                                    width="20"
                                    src={`/assets/${option.image.split("/")[2]}`}
                                    alt=""
                                />
                                {option.label.replaceAll("-", " ")}
                            </Box>
                        )}
                    />
                </div>
            </div>
            <div ref={chartContainerRef} className="chart-container" >
            </div>

        </div>
    );
}

export default App;
